#!/bin/sh
if [ "$EUID" -ne 0 ]
  then echo "Por gentileza, execute esse script como Sudo/Root"
  exit
fi

# Pegar usuario
USER=`who am i | awk '{print $1}'`
echo "Ola $USER =) ! \n"

# DIRETORIOS
BACKUP_DIR=/Users/$USER/Documents/_Backup
CENTRIFY_DIR=/private/var/centrifydc/reg/users/$USER/software/policies/centrify/centrifydc/settings/macmcx

# Cria ditório de Backup
echo "# log: criando ditório de backup: $BACKUP_DIR"
mkdir -p $BACKUP_DIR

# Colocar usuario como admin
echo "# log: Colocando $USER como administrador."
dscl . -append /Groups/admin GroupMembership $USER
LINE="$USER          ALL = (ALL) ALL"
FILE=/etc/sudoers
grep -q "^$LINE" "$FILE" || echo "$LINE" >> "$FILE"

# Desabilita centrifydc auto update
echo "# log: Desabilitando Atualizaçáo Continua do Centrify"
FILE=/etc/centrifydc/centrifydc.conf
LINE='gp.disable.user: true'
grep -q "^$LINE" "$FILE" || echo "$LINE" >> "$FILE"

# Habilitando multipla conexão de rede.
echo "# log: Habilitando conexão wifi ecabo simultaneo"
rsync -avz --remove-source-files /usr/local/share/centrifydc/mappers/machine/mac_mapper_generic.pl $BACKUP_DIR >/dev/null 2>&1
rsync -avz --remove-source-files /usr/local/share/centrifydc/mappers/machine/mac_mapper_network.pl $BACKUP_DIR >/dev/null 2>&1

# Habilitando acesso os System Preferences
echo "# log: Habilitando System Preferences"
rsync -avz --remove-source-files $CENTRIFY_DIR/systempreferences $BACKUP_DIR >/dev/null 2>&1

# Habilitando acesso aos Aplicativos do Macosx e Finder
echo "# log: Habilitando Aplicativos"
rsync -avz --remove-source-files $CENTRIFY_DIR/applicationaccess $BACKUP_DIR >/dev/null 2>&1
rsync -avz --remove-source-files $CENTRIFY_DIR/finder $BACKUP_DIR >/dev/null 2>&1

# Habilitando acesso a Midias
echo "# log: Habilitando Midia (like USB)"
rsync -avz --remove-source-files $CENTRIFY_DIR/mediaaccess $BACKUP_DIR >/dev/null 2>&1

# Order network Order
echo "# log: Ordenando serviços de rede para Wi-Fi ter prioridade"
IFS=$'\t\n'
LIST_SERVICES=$(echo `for i in $(networksetup -listallnetworkservices | grep -v "An asterisk" | grep -v "Wi-Fi"); do echo "\"$i\""; done;` | paste -sd " " -)
echo "networksetup -ordernetworkservices \"Wi-Fi\" $LIST_SERVICES" | bash

# Criar rotas quando estiver conectado na rede RC-Guest
echo "# log: Criando scripts de rotas quando conectar no RC-Guest"
echo "\033[1;33m # info: Quando estiver na Guest e Cabo - Rotas serão criadas e proxy desabilitado no Wi-fi."
echo "\033[1;33m # info: Quando sair da Guest, rotas serão destruidas e proxy habilitado no Wi-fi."
touch /var/log/rede_routes.log
chmod 777 /var/log/rede_routes.log

chmod +x /usr/local/bin/rede_wifi_routes.sh

cp ./scripts/rede.watch.wifi.plist /Library/LaunchAgents/
cp ./scripts/rede_wifi_routes.sh /usr/local/bin/

launchctl unload /Library/LaunchAgents/rede.watch.wifi.plist
launchctl load /Library/LaunchAgents/rede.watch.wifi.plist

# Configura conexão automatica na rede Guest
echo "# log: Criando arquivo para configurar usuario e senha do Wifi Guest"
echo "\033[1;33m # info: Editar arquivo /etc/.rede_wifi_guest"
touch /etc/.rede_wifi_guest
chmod 777 /etc/.rede_wifi_guest
echo "email" >> /etc/.rede_wifi_guest
echo "senha" >> /etc/.rede_wifi_guest

# Configurar Hosts
echo "# log: Configurando /etc/hosts para acessar outlook e lync enquanto no wifi"
FILE=/etc/hosts
LINE='172.20.202.55 cas.redecardsa.rccorp.net'
grep -q "^$LINE" "$FILE" || echo "$LINE" >> "$FILE"
LINE='172.20.202.35 fe.redecardsa.rccorp.net sip'
grep -q "^$LINE" "$FILE" || echo "$LINE" >> "$FILE"
