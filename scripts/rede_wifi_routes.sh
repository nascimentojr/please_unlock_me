#!/bin/sh
# Se voce evoluir esse script, nao esquece de compartilhar com os brodis. =)

echo 'Hey.' > /var/log/rede_routes.log

ip_route=`netstat -rn | grep default | grep 172 | awk '{print $2}'`
guest_user=`sed '1q;d' /etc/.rede_wifi_guest`
guest_pass=`sed '2q;d' /etc/.rede_wifi_guest`

wifi_name=$(/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk '/ SSID/ {print substr($0, index($0, $2))}')

create_routes(){
	route add 172.20.202.35 $ip_route
	route add 172.20.202.55 $ip_route
	route add 172.20.202.15 $ip_route
	route add 172.20.192.0/20 $ip_route
	route add 10/8 $ip_route
}
destroy_routes(){
	route delete 172.20.202.35 $ip_route
	route delete 172.20.202.55 $ip_route
	route delete 172.20.202.15 $ip_route
	route delete 172.20.192.0/20 $ip_route
	route delete 10/8 $ip_route
}
enable_proxy(){
	networksetup -setstreamingproxystate wi-fi on
	networksetup -setftpproxystate wi-fi on
	networksetup -setwebproxystate wi-fi on
	networksetup -setsecurewebproxystate wi-fi on
	networksetup -setsocksfirewallproxystate wi-fi on
	networksetup -setgopherproxystate wi-fi on
}
disable_proxy(){
	networksetup -setstreamingproxystate wi-fi off
	networksetup -setftpproxystate wi-fi off
	networksetup -setwebproxystate wi-fi off
	networksetup -setsecurewebproxystate wi-fi off
	networksetup -setsocksfirewallproxystate wi-fi off
	networksetup -setgopherproxystate wi-fi off
}

if [ $wifi_name == "RC-GUEST" ]; then
	# Create Routes
	create_routes

	# Faz login no Wifi
	sleep 10
	curl 'https://1.1.1.1/login.html' -H 'Origin: https://1.1.1.1' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,es-ES;q=0.6,es;q=0.5' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: https://1.1.1.1/login.html?redirect=1.1.1.1/login.html' -H 'Connection: keep-alive' --data "buttonClicked=4&err_flag=0&err_msg=&info_flag=0&info_msg=&redirect_url=http%3A%2F%2F1.1.1.1%2Flogin.html&network_name=Guest+Network&username=$guest_user&password=$guest_pass" --compressed --insecure

	# Desabilita o Proxy do WIFI
	disable_proxy

	echo 'Oi - Wifi RC-GUEST.' > /var/log/rede_routes.log

elif [ $wifi_name == "RC-WLAN"  ]; then
	# Destroy Routes
	destroy_routes

	# Habilita o Proxy do WIFI
	enable_proxy

	echo 'Oi - Wifi RC-WCLAN.' > /var/log/rede_routes.log
else
	# Destroy Routes
	destroy_routes

	# Desabilita o Proxy do WIFI
	disable_proxy

	echo 'Oi - Wifi Others.' > /var/log/rede_routes.log
fi
